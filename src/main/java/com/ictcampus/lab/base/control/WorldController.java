package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.model.WorldRequest;
import com.ictcampus.lab.base.control.model.WorldResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@RestController
@RequestMapping( "/api/v1/worlds" )
@AllArgsConstructor
public class WorldController {
	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<WorldResponse> getWorlds() {
		List<WorldResponse> list = new ArrayList<>();

		WorldResponse worldResponse = new WorldResponse();
		worldResponse.setId( 1L );
		worldResponse.setName( "Terra" );
		worldResponse.setSystem( "Sistema Solare" );
		list.add( worldResponse );

		WorldResponse worldResponse1 = new WorldResponse();
		worldResponse1.setId( 2L );
		worldResponse1.setName( "Marte" );
		worldResponse1.setSystem( "Sistema Solare" );
		list.add( worldResponse1 );

		WorldResponse worldResponse2 = new WorldResponse();
		worldResponse2.setId( 3L );
		worldResponse2.setName( "Giove" );
		worldResponse2.setSystem( "Sistema Solare" );
		list.add( worldResponse2 );

		WorldResponse worldResponse3 = new WorldResponse();
		worldResponse3.setId( 4L );
		worldResponse3.setName( "Saturno" );
		worldResponse3.setSystem( "Sistema Solare" );
		list.add( worldResponse3 );
		return list;
	}

	@GetMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public WorldResponse getWorld(
			@PathVariable(name = "id") Long id
	) {
		WorldResponse worldResponse = new WorldResponse();
		worldResponse.setId( 1L );
		worldResponse.setName( "Terra" );
		worldResponse.setSystem( "Sistema Solare" );

		return worldResponse;
	}

	@PostMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public Long createWorld(
			@RequestBody WorldRequest worldRequest
	) {
		WorldResponse worldResponse = new WorldResponse();
		worldResponse.setId( 1L );
		worldResponse.setName( worldRequest.getName() );
		worldResponse.setSystem( worldRequest.getSystem() );

		return worldResponse.getId();
	}

	@PutMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void editWorld(
			@PathVariable(name = "id") Long id,
			@RequestBody WorldRequest worldRequest
	) {
		WorldResponse worldResponse = new WorldResponse();
		worldResponse.setId( id );
		worldResponse.setName( worldRequest.getName() );
		worldResponse.setSystem( worldRequest.getSystem() );
	}

	@DeleteMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void deleteWorld(
			@PathVariable(name = "id") Long id
	) {
		WorldResponse worldResponse = new WorldResponse();
		worldResponse.setId( id );
	}
}
